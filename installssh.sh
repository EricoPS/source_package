#!/bin/sh

echo "install ssh"

wget https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/openssh/1:8.2p1-4ubuntu0.4/openssh_8.2p1.orig.tar.gz

tar zxvf openssh_8.2p1.orig.tar.gz

cd openssh

./configure

make && make tests
